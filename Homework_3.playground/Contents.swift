//: Сортировка массива с помощью замыкания, сначала в одну сторону, затем в другую

let array = [1, 4, 2, 3, 5, 7, 13, 24, 59, 20, 11, 35]
let sortedArray_1 = array.sorted(by: < )
let sortedArray_2 = array.sorted(by: > )

print("Отсортированный массив (возр.): \(sortedArray_1),\nотсортированный массив (убыв.): \(sortedArray_2) ")


//: Метод который запрашивает имена друзей, после этого имена кладутся в массив

func addNames(name: String) {
    arrayOfNames.append(name)
    arrayOfNames.sort(by: {$0.count < $1.count})
}

var arrayOfNames = [String]()

addNames(name: "Олег")
addNames(name: "Артем")
addNames(name: "Андрей")

print("Отсортированнный массив по возрастанию количества букв в имени\(arrayOfNames)")


//: Написать функцию которая будет принимать ключи, выводить ключи и значения словаря

func addItems(key: Int, value: String){
    dictionaryOfNames.updateValue(value, forKey: key)
}

var dictionaryOfNames = [Int: String]()

addItems(key: 1, value: "vsdvs")
addItems(key: 2, value: "bbd")
print(dictionaryOfNames)


//: Написать функцию, которая принимает пустые массивы

func checkArrays( tuple: ([String], [Int]), closure_1: ([String]) -> Bool, closure_2: ([Int]) -> Bool) {
    
    if closure_1(tuple.0) {
        tupleOfArrays.0.append("newItem")
        print(tupleOfArrays.0)
    }
    if closure_2(tuple.1) {
        tupleOfArrays.1.append(1)
        print(tupleOfArrays.1)
    }
 
}

var tupleOfArrays = ([String](), [Int]())

checkArrays(tuple: tupleOfArrays, closure_1: {_ in tupleOfArrays.0.isEmpty}, closure_2: {_ in tupleOfArrays.1.isEmpty})



